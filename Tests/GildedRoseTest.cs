﻿using System.Collections.Generic;
using System.Linq;
using csharp.ItemWrappers;
using NUnit.Framework;

namespace csharp.Tests {

/*
- All items have a SellIn value which denotes the number of days we have to sell the item
- All items have a Quality value which denotes how valuable the item is

Rules:
    (OK) At the end of each day our system lowers both (SellIn and Quality) values for every item
    (OK) Once the sell by date has passed, Quality degrades twice as fast
	(OK) The Quality of an item is never negative
	(OK) The Quality of an item is never more than 50
	(OK) "Aged Brie" actually increases in Quality the older it gets
	(OK) "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
	(OK) "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
	     Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but 
	     Quality drops to 0 after the concert
    (OK) "Conjured" items degrade in Quality twice as fast as normal items
*/

    [TestFixture]
    public class GildedRoseTest {

        private GildedRose _gildedRose;
        private IList<ItemWrapper> _wrappers;

        [SetUp]
        public void SetUp() {
            _wrappers = new List<ItemWrapper>();
            _gildedRose = new GildedRose(_wrappers);
        }

        [Test]
        public void SellInDegradesAtTheEndOfTheDay() {
            const int sellIn = 4;
            const int quality = 10;
            const int expectedSellIn = sellIn - 1;
            var anItem = AnItem(sellIn, quality);
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedSellIn, anItem.SellIn);
        }

        [Test]
        public void QualityDegradesAtTheEndOfTheDay() {
            const int sellIn = 4;
            const int quality = 10;
            const int expectedQuality = quality - 1;
            var anItem = AnItem(sellIn, quality);
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedQuality, anItem.Quality);
        }

        [Test]
        public void QualityDegradesTwiceAsFastWhenTheSellDateHasPassed() {
            const int sellIn = 0;
            const int quality = 10;
            const int expectedQuality = quality - 2;
            var anItem = AnItem(sellIn, quality);
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedQuality, anItem.Quality);
        }

        [Test]
        public void TheQualityOfAnItemIsNeverNegative() {
            const int sellIn = 0;
            const int quality = 1;
            var anItem = AnItem(sellIn, quality);
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.Greater(anItem.Quality, -1);
        }

        [Test]
        public void AgedBrieIncreasesInQualityTheOlderItGets() {
            const int sellIn = 0;
            const int quality = 8;
            const int expectedQuality = quality + 2;
            var anItem = AnItem(sellIn, quality, "Aged Brie");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedQuality, anItem.Quality);
        }

        [Test]
        public void TheQualityOfAnItemIsNeverMoreThan50() {
            const int sellIn = 0;
            const int quality = 50;
            var anItem = AnItem(sellIn, quality, "Aged Brie");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(quality, anItem.Quality);
        }

        [Test]
        public void SulfurasNeverHasToBeSold() {
            const int sellIn = 5;
            const int quality = 80;
            var anItem = AnItem(sellIn, quality, "Sulfuras, Hand of Ragnaros");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(sellIn, anItem.SellIn);
        }

        [Test]
        public void SulfurasNeverDecreasesInQuality() {
            const int sellIn = -1;
            const int quality = 80;
            var anItem = AnItem(sellIn, quality, "Sulfuras, Hand of Ragnaros");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(quality, anItem.Quality);
        }

        [Test]
        public void BackstagePassesQualityIncreasesWhenThereAreMoreThan_10_DaysToTheConcert() {
            const int sellIn = 12;
            const int quality = 40;
            const int expectedQuality = quality + 1;
            var anItem = AnItem(sellIn, quality, "Backstage passes to a TAFKAL80ETC concert");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedQuality, anItem.Quality);
        }

        [Test]
        [TestCase(10)]
        [TestCase(7)]
        public void BackstagePassesQualityIncreasesBy_2_WhenThereAre_10_DaysOrLessToTheConcert(int sellIn) {
            const int quality = 40;
            const int expectedQuality = quality + 2;
            var anItem = AnItem(sellIn, quality, "Backstage passes to a TAFKAL80ETC concert");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedQuality, anItem.Quality);
        }

        [Test]
        [TestCase(5)]
        [TestCase(3)]
        public void BackstagePassesQualityIncreasesBy_3_WhenThereAre_5_DaysOrLessToTheConcert(int sellIn) {
            const int quality = 40;
            const int expectedQuality = quality + 3;
            var anItem = AnItem(sellIn, quality, "Backstage passes to a TAFKAL80ETC concert");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedQuality, anItem.Quality);
        }

        [Test]
        public void BackstagePassesQualityDropsTo_0_AfterTheConcert() {
            const int sellIn = 0;
            const int quality = 40;
            const int expectedQuality = quality - quality;
            var anItem = AnItem(sellIn, quality, "Backstage passes to a TAFKAL80ETC concert");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedQuality, anItem.Quality);
        }

        [Test]
        public void ConjuredItemsDegradeInQualityTwiceAsFastAsNormalItemsWhenTheSellDateHasNotPassed() {
            const int sellIn = 5;
            const int quality = 10;
            const int expectedQuality = quality - 2;
            var anItem = AnItem(sellIn, quality, "Conjured");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedQuality, anItem.Quality);
        }

        [Test]
        public void ConjuredItemsDegradeInQualityTwiceAsFastAsNormalItemsWhenTheSellDateHasPassed() {
            const int sellIn = 0;
            const int quality = 10;
            const int expectedQuality = quality - 4;
            var anItem = AnItem(sellIn, quality, "Conjured");
            _wrappers.Add(AWrapperFor(anItem));
            _gildedRose.UpdateQuality();
            Assert.AreEqual(expectedQuality, anItem.Quality);
        }

        private static ItemWrapper AWrapperFor(Item item) {
            return WrapperFactory.CreateWrapper(item);
        }

        private static Item AnItem(int sellIn, int quality, string name = "anItem") {
            return new Item {
                Name = name,
                SellIn = sellIn,
                Quality = quality
            };
        }

    }

}