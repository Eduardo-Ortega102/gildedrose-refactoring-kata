﻿using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using ApprovalUtilities.Utilities;
using csharp.ItemWrappers;

namespace csharp {

    public class GildedRose {

        private readonly IList<ItemWrapper> _wrappers;

        public GildedRose(IList<Item> items) {
            _wrappers = new List<ItemWrapper>();
            foreach (var item in items)
                _wrappers.Add(WrapperFactory.CreateWrapper(item));
        }

        public GildedRose(IList<ItemWrapper> wrappers) {
            _wrappers = wrappers;
        }

        public void UpdateQuality() {
            _wrappers.ForEach(wrapper => wrapper.UpdateQuality());
        }

    }

}