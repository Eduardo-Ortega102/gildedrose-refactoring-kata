﻿namespace csharp.ItemWrappers {

    public class AgedBrieWrapper : ItemWrapper {

        public AgedBrieWrapper(Item item) : base(item) {
        }

        public override void UpdateQuality() {
            DecreaseSellIn();
            IncreaseQuality(2);
        }

    }

}