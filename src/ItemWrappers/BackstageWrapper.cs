﻿namespace csharp.ItemWrappers {

    public class BackstageWrapper : ItemWrapper {

        public BackstageWrapper(Item item) : base(item) {
        }

        public override void UpdateQuality() {
            DecreaseSellIn();
            if (Item.SellIn <= 0) DecreaseQuality(Item.Quality);
            else IncreaseQuality(GetIncrement());
        }

        private int GetIncrement() {
            return Item.SellIn > 0 && Item.SellIn <= 5 ? 3 
                 : Item.SellIn > 5 && Item.SellIn <= 10 ? 2 
                 : 1;
        }

    }

}