﻿namespace csharp.ItemWrappers {

    public class ConjuredWrapper : ItemWrapper {

        public ConjuredWrapper(Item item) : base(item) {
        }

        public override void UpdateQuality() {
            DecreaseSellIn();
            DecreaseQuality(Item.SellIn <= 0 ? 4 : 2);
        }

    }

}