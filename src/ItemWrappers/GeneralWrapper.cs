namespace csharp.ItemWrappers {

    public class GeneralWrapper : ItemWrapper {

        public GeneralWrapper(Item item) : base(item) {
        }

        public override void UpdateQuality() {
            DecreaseSellIn();
            DecreaseQuality(Item.SellIn <= 0 ? 2 : 1);
        }

    }

}