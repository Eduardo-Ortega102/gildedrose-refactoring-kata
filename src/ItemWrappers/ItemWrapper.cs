﻿namespace csharp.ItemWrappers {

    public abstract class ItemWrapper {

        protected Item Item;

        protected ItemWrapper(Item item) {
            Item = item;
        }

        public abstract void UpdateQuality();

        protected void DecreaseSellIn() {
            Item.SellIn = Item.SellIn - 1;
        }

        protected void DecreaseQuality(int decrement) {
            if (Item.Quality == 0) return;
            Item.Quality = Item.Quality - decrement;
            if (Item.Quality < 0) Item.Quality = 0;
        }

        protected void IncreaseQuality(int increment) {
            if (Item.Quality == 50) return;
            Item.Quality = Item.Quality + increment;
            if (Item.Quality > 50) Item.Quality = 50;
        }

    }

}