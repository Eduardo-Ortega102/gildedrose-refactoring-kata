using csharp.ItemWrappers;

namespace csharp {

    public class WrapperFactory {

        public static ItemWrapper CreateWrapper(Item item) {
            if (item.Name.Equals("Aged Brie")) return new AgedBrieWrapper(item);
            if (item.Name.Equals("Backstage passes to a TAFKAL80ETC concert")) return new BackstageWrapper(item);
            if (item.Name.Equals("Conjured")) return new ConjuredWrapper(item);
            if (item.Name.Equals("Sulfuras, Hand of Ragnaros")) return new SulfurasWrapper(item);
            return new GeneralWrapper(item);
        }

    }

}